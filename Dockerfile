FROM node:18-alpine as build

WORKDIR /tour-of-heroes

# RUN npm install -g @angular/cli

COPY package.json . 

RUN npm install

COPY . .

RUN npm run build

FROM nginx

COPY --from=build /tour-of-heroes/dist/angular-tour-of-heroes /usr/share/nginx/html

COPY nginx.conf /etc/nginx/conf.d/default.conf
